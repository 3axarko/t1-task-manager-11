package ru.t1.zkovalenko.tm.controller;

import ru.t1.zkovalenko.tm.api.controller.IProjectController;
import ru.t1.zkovalenko.tm.api.service.IProjectService;
import ru.t1.zkovalenko.tm.constant.ConsoleColorConst;
import ru.t1.zkovalenko.tm.model.Project;
import ru.t1.zkovalenko.tm.util.ColorizeConsoleTextUtil;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject(){
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();

        final Project project = projectService.create(name, description);
        if (project == null) System.err.println("[ERROR]\nName is required field");
        else ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void clearProjects(){
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void showProjects(){
        System.out.println("[PROJECT LIST]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for(final Project project: projects){
            System.out.println(index + ". " + project);
            index++;
        }
        ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void showProject(final Project project){
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    @Override
    public void showProjectById(){
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null){
            System.err.println("[FAIL]");
            return;
        }
        showProject(project);
        ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void showProjectByIndex(){
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null){
            System.err.println("[FAIL]");
            return;
        }
        showProject(project);
        ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void removeProjectById(){
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeById(id);
        if (project == null) System.err.println("[FAIL]");
        else ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void removeProjectByIndex(){
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeByIndex(index);
        if (project == null) System.err.println("[FAIL]");
        else ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void updateProjectById(){
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateById(id, name, description);
        if (project == null) System.err.println("[FAIL]");
        else ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void updateProjectByIndex(){
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateByIndex(index, name, description);
        if (project == null) System.err.println("[FAIL]");
        else ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

}
